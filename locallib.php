<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk user registration functions
 *
 * @package    tool
 * @subpackage uploaduser
 * @copyright  2004 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define('ACTION_BULK', 'bulk');
define('ACTION_MANUAL', 'manual');


class bulk_action_manger {

    public function load_actions() {

    }
}


function render_singleselct($str, $label) {
    echo '<div class="form-group row fitem  ">
        <div class="col-md-3">
        <span class="pull-xs-right text-nowrap"></span>
        <label class="col-form-label d-inline " for="id_email">
        '.$label.'
        </label>
        </div>
        <div class="col-md-9 form-inline felement" data-fieldtype="text">';
    echo $str;
    echo '<div class="form-control-feedback" id="id_error_email" style="display: none;">
        </div>
        </div>
        </div>';
}
