<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk user registration script from a comma separated file
 *
 * @package    tool
 * @subpackage uploaduser
 * @copyright  2004 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../../config.php');
require('locallib.php');
require_once $CFG->libdir.'/formslib.php';
require_once($CFG->libdir.'/csvlib.class.php');

core_php_time_limit::raise(60*60); // 1 hour should be enough
raise_memory_limit(MEMORY_HUGE);

require_login(0, false);
//require_capability('moodle/site:supporttool', context_system::instance());

$action = optional_param('action', null, PARAM_RAW);
$type = optional_param('type', '', PARAM_RAW);

$params = [];
if($action) {
    $type = empty($type) ? ACTION_BULK : $type;
    $params = [
        'action' => $action,
        'type' => $type
    ];
}
// Start making page
$PAGE->set_context(context_system::instance());
$PAGE->set_url('/admin/tool/supporttool/index.php', $params);
$PAGE->set_title('Bulk upload');
$PAGE->set_pagelayout('admin');
$PAGE->navbar->add('admin');

if($action) {
    // Active plugins
    $types = core_component::get_plugin_types();
    $mform = new \tool_supporttool\bulk_action_form(null, $params);

    // Form post data
    if($data = $mform->get_data()) {
        $class = '\supporttoolaction_'.$action.'\\'.$action;
        $class = new $class();

        if($data->type == ACTION_BULK) {
            $iid = csv_import_reader::get_new_iid('uploaduser');
            $cir = new csv_import_reader($iid, 'uploaduser');
            $content = $mform->get_file_content('userfile');
            $readcount = $cir->load_csv_content($content, $data->encoding, $data->delimiter_name);
            $csvloaderror = $cir->get_error();
            // Sent error if its
            if($csvloaderror)
                print_error($csvloaderror);

            $class->bulk_process($cir, $data);
        } else if($data->type == ACTION_MANUAL) {
            $class->manual_process($data);
        }
    }
}
// Bulk, Manual types
$types = array(
    ACTION_BULK => get_string(ACTION_BULK, 'tool_supporttool'),
    ACTION_MANUAL => get_string(ACTION_MANUAL, 'tool_supporttool'),
);

$plugins = core_plugin_manager::instance()->get_plugins_of_type('supporttoolaction');

foreach($plugins as $plugin) {
    $options[$plugin->name] = $plugin->displayname;
}

echo $OUTPUT->header();

render_singleselct($OUTPUT->single_select($PAGE->url, 'action', $options, $action), 'Select action');
$url = new moodle_url($CFG->wwwroot.'/admin/tool/supporttool/index.php', array('action' => $action));
render_singleselct($OUTPUT->single_select($url, 'type', $types, $type), 'Select type');
if($action) {
    $mform->display();
}
echo $OUTPUT->footer();
