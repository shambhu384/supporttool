<?php

require '../../../config.php';

$obj = new \supporttoolaction_bulkenrolment\bulkenrolment();

$data = [
    [
        'scott', 'test'
    ]
];

$obj->manual_process($data);
$obj->bulk_process($data);
