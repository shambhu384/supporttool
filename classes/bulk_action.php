<?php

namespace tool_supporttool;

/**
 * Parent class of all actions
 *
 */

abstract class bulk_action {

    /**
     * Building form fields
     *
     * @param HTMLQuick Form
     * @param string
     */
    abstract public function get_options(&$mform, $type);

    /**
     * Manual form actoin
     *
     */
    abstract public function manual_process($csvdata);

    /**
     * Bulk action
     *
     * @param
     */
    abstract public function bulk_process($csvdata, $formdata);
}
