<?php

namespace tool_supporttool;

class bulk_action_form extends \moodleform{

    public function definition() {
        global $CFG;
        $mform = $this->_form;
        $plugins = \core_plugin_manager::instance();
        $options = $plugins->get_subplugins_of_plugin('tool_supporttool');


        if($this->_customdata['type'] == ACTION_BULK) {
            $mform->addElement('filepicker', 'userfile', get_string('file'));
            $mform->addRule('userfile', null, 'required');

            $choices = \csv_import_reader::get_delimiter_list();
            $mform->addElement('select', 'delimiter_name', get_string('csvdelimiter', 'tool_uploaduser'), $choices);
            if (array_key_exists('cfg', $choices)) {
                $mform->setDefault('delimiter_name', 'cfg');
            } else if (get_string('listsep', 'langconfig') == ';') {
                $mform->setDefault('delimiter_name', 'semicolon');
            } else {
                $mform->setDefault('delimiter_name', 'comma');
            }
            $choices = \core_text::get_encodings();
            $mform->addElement('select', 'encoding', get_string('encoding', 'tool_uploaduser'), $choices);
            $mform->setDefault('encoding', 'UTF-8');
        }
        // Add custom elements in below
        $this->add_custom_elements();

        // add buttons
        $this->add_action_buttons();
    }

    /**
     * Load custom form elements
     */
    public function add_custom_elements() {

        $class = $this->_customdata['action'];
        $class = '\supporttoolaction_'.$class.'\\'.$class;

        $actionclass = new $class();
        $actionclass->get_options($this->_form, $this->_customdata['type']);

        // Hidden element
        $this->_form->addElement('hidden', 'type', $this->_customdata['type']);
        $this->_form->setType('type', PARAM_RAW);

        $this->_form->addElement('hidden', 'action', $this->_customdata['action']);
        $this->_form->setType('action', PARAM_RAW);
    }

    public function validation($data, $files) {
        $errors = array();
        $class = $data['action'];
        $class = '\supporttoolaction_'.$class.'\\'.$class;

        $actionclass = new $class();
        if(method_exists($actionclass, 'validation')) {
            $referrors = $actionclass->validation($data, $files);
            array_merge($errors, $referrors);
        }
        return $errors;
    }
}
