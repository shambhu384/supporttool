<?php

namespace supporttoolaction_setpassword;

class setpassword extends \tool_supporttool\bulk_action {

    public function get_options(&$mform, $type) {
        global $PAGE,$DB;

        if($type == ACTION_MANUAL) {
            $users = $DB->get_records_menu('user', array('deleted' => 0 , 'suspended' => 0), '','id, concat(firstname , lastname) as fullname');
            $select = $mform->addElement('select', 'user', get_string('user','supporttoolaction_setpassword'), $users);
            $select->setMultiple(true);

            $mform->addElement('text', 'setpw', get_string('setpw', 'supporttoolaction_setpassword'));
        } else if($type == ACTION_BULK) {
            $mform->addElement('text', 'setpw', get_string('setpw', 'supporttoolaction_setpassword'));
        }
    }

    /**
     *  USER generate
     *
     * @param array [[userid,courseid,startdate,endate]]
     *
     *
     */
    public function manual_process($cir) {
        global $CFG ,$DB;

        $userval = $cir->user[0];
        $user = $DB->get_record('user',array('id' => $userval));
        //print_object($user); die(); // DONOTCOMMIT
        $lang = empty($user->lang) ? $CFG->lang : $user->lang;

        $site  = get_site();

        $supportuser = \core_user::get_support_user();

        $newpassword = $cir->setpw;

        update_internal_user_password($user, $newpassword,false);

        $a = new \stdClass();
        $a->firstname   = fullname($user, true);
        $a->sitename    = format_string($site->fullname);
        $a->username    = $user->username;
        $a->newpassword = $newpassword;
        $a->link        = $CFG->wwwroot .'/login/';
        $a->signoff     = generate_email_signoff();

        $message = (string)new \lang_string('autosetpasswordtext', '', $a, $lang);

        $subject = format_string($site->fullname) .': '. (string)new \lang_string('autosetpasswordsub', '', $a, $lang);

        // Directly email rather than using the messaging system to ensure its not routed to a popup or jabber.
        email_to_user($user, $supportuser, $subject, $message);
        echo '<div class="alert alert-success">Sucess</div>';
    }

    /**
     * Bluk process
     *
     */
    public function bulk_process($cir, $formdata) {
        global $CFG,$DB;

        // init csv import helper
        $cir->init();
        $linenum = 1;
        // init upload progress tracker
        $validation = array();
        while ($line = $cir->next()) {
            $linenum++;
            $user = new \stdclass();
            // add fields to user object
            foreach ($line as $keynum => $value) {
                $user = $DB->get_record('user',array('username' => $value));
                $lang = empty($user->lang) ? $CFG->lang : $user->lang;

                $site  = get_site();

                $supportuser = \core_user::get_support_user();

                $newpassword = $formdata->setpw;

                update_internal_user_password($user, $newpassword,false);

                $a = new \stdClass();
                $a->firstname   = fullname($user, true);
                $a->sitename    = format_string($site->fullname);
                $a->username    = $user->username;
                $a->newpassword = $newpassword;
                $a->link        = $CFG->wwwroot .'/login/';
                $a->signoff     = generate_email_signoff();

                $message = (string)new \lang_string('autosetpasswordtext', '', $a, $lang);

                $subject = format_string($site->fullname) .': '. (string)new \lang_string('autosetpasswordsub', '', $a, $lang);

                // Directly email rather than using the messaging system to ensure its not routed to // a popup or jabber.
                email_to_user($user, $supportuser, $subject, $message);
            }
            echo '<div class="alert alert-success">Sucess</div>';
        }
    }
}
