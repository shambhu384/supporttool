<?php

namespace supporttoolaction_sendinvitation;

class sendinvitation extends \tool_supporttool\bulk_action {

    public function get_options(&$mform, $type) {
        global $PAGE,$DB;

        if($type == ACTION_MANUAL) {
        } else if($type == ACTION_BULK) {
            $mform->addElement('editor', 'body', get_string('body', 'supporttoolaction_sendinvitation'));
            $mform->setType('body', PARAM_RAW);
        }
    }

    /**
     *  USER generate
     *
     * @param array [[userid,courseid,startdate,endate]]
     *
     *
     */
    public function manual_process($cir) {
    }

    /**
     * Bluk process
     *
     */
    public function bulk_process($cir, $formdata) {
        global $CFG,$DB;
        // init csv import helper
        $cir->init();
        $linenum = 1;
        // init upload progress tracker
        $validation = array();
        while ($line = $cir->next()) {
            $linenum++;
            $user = new \stdclass();
            // add fields to user object
            foreach ($line as $keynum => $value) {
                $user = $DB->get_record('user',array('username' => $value));
                $from = \core_user::get_support_user();
                $a = new \stdClass();
                $a->name = fullname($user);
                $a->contact = $from->email;
                $a->signature = fullname($from);
                $subject = get_string('subject', 'supporttoolaction_sendinvitation', $a->name);
                $messagehtml = $formdata->body['text'];

                print_object($messagehtml); die(); // DONOTCOMMIT
                $messagetext = format_text_email($messagehtml, FORMAT_HTML);
                $sentuser = email_to_user($user, $from, $subject, $messagetext, $messagehtml);
            }
            echo '<div class="alert alert-success">Sucess</div>';
        }
    }
}
