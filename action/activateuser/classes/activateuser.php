<?php

namespace supporttoolaction_activateuser;

class activateuser extends \tool_supporttool\bulk_action {

    public function get_options(&$mform, $type) {
        global $PAGE,$DB;

        if($type == ACTION_MANUAL) {
            $users = $DB->get_records_menu('user', array('deleted' => 0,'suspended' => 1), '','id, concat(firstname , lastname) as fullname');
            $select = $mform->addElement('select', 'user', get_string('user','supporttoolaction_activateuser'), $users);
            $select->setMultiple(true);
        } else if($type == ACTION_BULK) {
            // I don't have do anything
        }
    }

    /**
     *  USER generate
     *
     * @param array [[userid,courseid,startdate,endate]]
     *
     *
     */
    public function manual_process($cir) {
        global $USER, $CFG,$DB;
        require_once($CFG->dirroot.'/lib/moodlelib.php');
        require_once($CFG->dirroot . '/user/lib.php');
        $userval = $cir->user[0];
        if ($user = $DB->get_record('user', array('id' => $userval,
            'mnethostid' => $CFG->mnet_localhost_id, 'deleted' => 0, 'suspended' => 1))) {
                if ($user->suspended != 0) {
                    $user->suspended = 0;
                    user_update_user($user, false, true);
                    $from = \core_user::get_support_user();
                    $a = new \stdClass();
                    $a->name = fullname($user);
                    $a->contact = $from->email;
                    $a->signature = fullname($from);
                    $subject = get_string_manager()->get_string('email:user:unsuspend:subject',
                        'local_usermanagement', $a);
                    $messagehtml = get_string_manager()->get_string('email:user:unsuspend:manual:body',
                        'local_usermanagement', $a);
                    $messagetext = format_text_email($messagehtml, FORMAT_HTML);
                    email_to_user($user, $from, $subject, $messagetext, $messagehtml);
                }
        }
            echo '<div class="alert alert-success">Sucess</div>';
    }

    /**
     * Bluk process
     *
     */
    public function bulk_process($cir, $formdata) {
        global $CFG,$DB,$USER;
        require_once($CFG->dirroot.'/lib/moodlelib.php');
        require_once($CFG->dirroot . '/user/lib.php');
        // init csv import helper
        $cir->init();
        $linenum = 1;
        // init upload progress tracker
        $validation = array();
        while ($line = $cir->next()) {
            $linenum++;
            $user = new \stdclass();
            // add fields to user object
            foreach ($line as $keynum => $value) {
                if ($user = $DB->get_record('user', array('username' => $value,
                    'mnethostid' => $CFG->mnet_localhost_id, 'deleted' => 0, 'suspended' => 1))) {
                        if ($user->suspended != 0) {
                            $user->suspended = 0;
                            user_update_user($user, false, true);
                            $from = \core_user::get_support_user();
                            $a = new \stdClass();
                            $a->name = fullname($user);
                            $a->contact = $from->email;
                            $a->signature = fullname($from);
                            $subject = get_string_manager()->get_string('email:user:unsuspend:subject',
                                'local_usermanagement', $a);
                            $messagehtml = get_string_manager()->get_string('email:user:unsuspend:manual:body',
                                'local_usermanagement', $a);
                            $messagetext = format_text_email($messagehtml, FORMAT_HTML);
                            email_to_user($user, $from, $subject, $messagetext, $messagehtml);
                        }
                  }
            }
            echo '<div class="alert alert-success">Sucess</div>';
        }
    }
}
