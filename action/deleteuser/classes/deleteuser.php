<?php

namespace supporttoolaction_deleteuser;

class deleteuser extends \tool_supporttool\bulk_action {

    public function get_options(&$mform, $type) {
        global $PAGE,$DB;

        if($type == ACTION_MANUAL) {
            $users = $DB->get_records_menu('user', array('deleted' => 0 , 'suspended' => 0), '','id, concat(firstname , lastname) as fullname');
            $select = $mform->addElement('select', 'user', get_string('user','supporttoolaction_deleteuser'), $users);
            $select->setMultiple(true);
        } else if($type == ACTION_BULK) {
            // I don't have do anything
        }
    }

    /**
     *  USER generate
     *
     * @param array [[userid,courseid,startdate,endate]]
     *
     *
     */
    public function manual_process($cir) {
        global $USER, $CFG,$DB;
        require_once($CFG->dirroot.'/lib/moodlelib.php');
        require_once($CFG->dirroot . '/user/lib.php');
        $userval = $cir->user[0];
        $user = $DB->get_record('user',array('id' => $userval));
        if (!is_siteadmin($user) and $USER->id != $user->id and $user->suspended != 1) {
             $user = $DB->update_record('user', array('id' => $user->id , 'deleted' => 1),false);
             $from = \core_user::get_support_user();
             $a = new \stdClass();
             $a->name = fullname($user);
             $a->contact = $from->email;
             $a->signature = fullname($from);
             $subject = get_string('email:user:delete:subject', 'supporttoolaction_deleteuser', $a->name);

             $messagehtml = get_string_manager()->get_string('email:user:delete:manual:body', 'supporttoolaction_deleteuser', $a);
             $messagetext = format_text_email($messagehtml, FORMAT_HTML);
             $sentuser = email_to_user($user, $from, $subject, $messagetext, $messagehtml);
        }
        if($sentuser){
            echo '<div class="alert alert-success">Sucess</div>';
        }
    }

    /**
     * Bluk process
     *
     */
    public function bulk_process($cir, $formdata) {
        global $CFG,$DB,$USER;
        require_once($CFG->dirroot.'/lib/moodlelib.php');
        require_once($CFG->dirroot . '/user/lib.php');
        // init csv import helper
        $cir->init();
        $linenum = 1;
        // init upload progress tracker
        $validation = array();
        while ($line = $cir->next()) {
            $linenum++;
            $user = new \stdclass();
            // add fields to user object
            foreach ($line as $keynum => $value) {
             $user = $DB->get_record('user',array('username' => $value));
             if (!is_siteadmin($user) and $USER->id != $user->id and $user->suspended != 1) {
                 $user =$DB->update_record('user', array('id' => $user->id , 'deleted' => 1),false);
                 $from = \core_user::get_support_user();
                 $a = new \stdClass();
                 $a->name = fullname($user);
                 $a->contact = $from->email;
                 $a->signature = fullname($from);
                 $subject = get_string('email:user:delete:subject', 'supporttoolaction_deleteuser', $a->name);

                 $messagehtml = get_string_manager()->get_string('email:user:delete:manual:body', 'supporttoolaction_deleteuser', $a);
                 $messagetext = format_text_email($messagehtml, FORMAT_HTML);
                 $sentuser = email_to_user($user, $from, $subject, $messagetext, $messagehtml);
             }
            }
            echo '<div class="alert alert-success">Sucess</div>';
        }
    }
}
