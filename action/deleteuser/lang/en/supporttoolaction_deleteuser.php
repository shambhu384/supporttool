<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'tool_uploaduser', language 'en', branch 'MOODLE_22_STABLE'
 *
 * @package    tool
 * @subpackage uploaduser
 * @copyright  2011 Petr Skoda {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['pluginname'] = 'Delete User';
$string['plugin'] = 'Delete User';
$string['user'] = 'Select Users';
$string['email:user:delete:subject'] = 'Your account has been Deleted';
$string['email:user:delete:manual:body'] = '<p>Dear {$a->name}</p>
    <p>Your account has been deleted.</p>
    <p>If you feel this is unintended or want to have your account activated again,
    please contact {$a->contact}</p>
    <p>Regards<br/>{$a->signature}</p>';
