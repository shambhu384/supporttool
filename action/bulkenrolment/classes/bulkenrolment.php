<?php

namespace supporttoolaction_bulkenrolment;

class bulkenrolment extends \tool_supporttool\bulk_action {

    public function get_options(&$mform, $type) {
        global $PAGE,$DB;

        if($type == ACTION_MANUAL) {

            $courses = $DB->get_records_menu('course', null, null, 'id, fullname');
            unset($courses[1]);
            $mform->addElement('searchableselector', 'courses', get_string('course'), $courses, array('multiple'));
            $mform->addRule('courses',get_string('required'), 'required', null, 'client');
            $mform->setType('courses', PARAM_INT);

            $roles = $DB->get_records_menu('role', null, '', 'id, shortname');
            $mform->addElement('select', 'role', get_string('role'), $roles);
            $mform->setType('role', PARAM_INT);

            $users = $DB->get_records_menu('user', array('deleted' => 0, 'suspended' => 0), '', "id, concat(firstname, concat(' ' ,lastname), concat('-',email))");
            $select = $mform->addElement('searchableselector', 'users', get_string('name'), $users, array('multiple'));
            $select->setMultiple(true);
            $mform->addRule('users', get_string('required'), 'required', null, 'client');

            $mform->addElement('date_time_selector', 'starttime', get_string('starttime', 'supporttoolaction_bulkenrolment'));
            array(
                'startyear' => 2017,
                'stopyear'  => 2030,
                'timezone'  => 99,
                'optional'  => false
            );
            $mform->addElement('date_time_selector', 'endtime', get_string('endtime', 'supporttoolaction_bulkenrolment'));
            array(
                'startyear' => 2017,
                'stopyear'  => 2030,
                'timezone'  => 99,
                'optional'  => false
            );

            $mform->addElement('hidden', 'id');
            $mform->setType('id', PARAM_INT);

            $mform->addElement('hidden', 'action');
            $mform->setType('action', PARAM_ALPHANUMEXT);

        } else if($type == ACTION_BULK) {
            // I don't have do anything
            $courses = $DB->get_records_menu('course', null, null, 'id, fullname');
            unset($courses[1]);
            $mform->addElement('searchableselector', 'courses', get_string('course'), $courses, array('multiple'));
            $mform->addRule('courses',get_string('required'), 'required', null, 'client');
            $mform->setType('courses', PARAM_INT);
            
            $sql = "SELECT id, archetype from {role} where archetype in 
                ('editingteacher','teacher','student')"; 
            $roles = $DB->get_records_sql_menu($sql,null,IGNORE_MISSING);
           
            $option = array();
        
               $mform->addElement('select', 'role', get_string('role'), $roles);
               $mform->setType('role', PARAM_INT);

           
        }
    }

    /**
     *  USER enro
     *
     * @param array [[userid,courseid,startdate,endate]]
     *
     *
     */
    public function manual_process($formdata) {
        global $DB, $USER;
        $courseid = implode(',', $formdata->courses);
        /**
         * here we will check weather user is enrollerd or not
         * if he is enrolled take enrolled user data 
         */
        $sql = "SELECT e.id,e.courseid, group_concat(ue.userid) as users 
                        FROM {enrol} e
                    LEFT JOIN {user_enrolments} ue on e.id = ue.enrolid AND ue.timeend < NOW() 
WHERE e.courseid in ($courseid) AND e.enrol ='manual' AND e.status=0 
                    GROUP by e.courseid";
          $enrolledusers = $DB->get_records_sql($sql);

<<<<<<< HEAD
        print_object($cir);
        // init csv import helper
        $cir->init();
        $linenum = 1; //column header is first line
=======
print_object($enrolledusers);// die(); // DONOTCOMMIT
        /*here we will check instance id is prasent in courseid  if it is prasent then need*/ 
        $sql1 = "SELECT instanceid, id 
                    from {context} 
                    where contextlevel = 50 and instanceid in($courseid)";
        $context = $DB->get_records_sql_menu($sql1);
        $enrolusers = [];
        $roleassign = [];
        if($enrolledusers) {
            foreach($enrolledusers as $course) {
                /**
                 * here we are checking weather selecting user and enrollerd user already enrolled in a particular course,
                 * if it is not enrolled then take diffrence  and insert into data base 
                 */
                $diff = array_diff($formdata->users, explode(',', $course->users));
             //   print_object($diff);die();
                foreach($diff as $userid) {
                    $insert = new \stdClass();
                    $insert->enrolid =$course->id;
                    $insert->userid = $userid;
                    $insert->timestart = $formdata->starttime;
                    $insert->timeend = $formdata->endtime;
                    $insert->modifierid = $USER->id;
                    $insert->timecreated = time();
                    $enrolusers[] = $insert;
>>>>>>> courselevel

                    $insert2 = new \stdClass();
                    $insert2->roleid = $formdata->role;
                    $insert2->contextid = $context[$course->courseid];
                    $insert2->userid = $userid;
                    $insert2->modifierid = $USER->id;
                    $roleassign[] = $insert2;
                }
            }
        }
        
        /**
         * inserting values into user_enrolments table
         */
        if(count($enrolledusers) > 0) {
            $DB->insert_records('user_enrolments', $enrolusers);
        }
        /**
         *inserting values into role assignment tables 
         */
        if(count($roleassign) > 0) {
            $DB->insert_records('role_assignments', $roleassign);
        }
    }
    /**
     * Bluk process
     *
     */
    public function bulk_process($cir, $formdata) {
        global $CFG,$DB;
        require_once($CFG->dirroot.'/group/lib.php');
        //print_object($courseid); die(); // DONOTCOMMIT

        $cir->init();
        $linenum = 1;
        // init upload progress tracker
        $validation = array();
        while ($line = $cir->next()) {
            $linenum++;
            $user = new \stdclass();
            // add fields to user object
            if(empty($line)) {
                continue;
            }


            $line[0] = str_replace('"', '', trim($line[0]));
            $line[1] = str_replace('"', '', trim($line[1]));
            $line[2] = str_replace('"', '', trim($line[2]));
          //  echo $line[0]."<br>".$line[1]."<br>".$line[2];

            foreach ($line as $keynum => $value) {
                print_object($value);

            }
            $sql = "SELECT e.id,e.courseid, group_concat(ue.userid) as users 
                        FROM {enrol} e
                    LEFT JOIN {user_enrolments} ue on e.id = ue.enrolid 
                    WHERE e.courseid in ($courseid) AND e.enrol ='manual' AND e.status=0 AND ue.timeend < NOW()
                    GROUP by e.courseid";
            $enrolledusers = $DB->get_records_sql($sql);

            print_object($enrolledusers); die(); // DONOTCOMMIT
        /*here we will check instance id is prasent in courseid  if it is prasent then need*/ 
       /* $sql1 = "SELECT instanceid, id 
                    from {context} 
                    where contextlevel = 50 and instanceid in($courseid)";
$context = $DB->get_records_sql_menu($sql1);*/

            
        }
    }

    public function validation($data, $files) {
        $errors = [];
        return $errors;
    }
}
