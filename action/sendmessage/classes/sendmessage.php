<?php

namespace supporttoolaction_sendmessage;

class sendmessage extends \tool_supporttool\bulk_action {

    public function get_options(&$mform, $type) {
        global $PAGE,$DB;

        if($type == ACTION_MANUAL) {
            $users = $DB->get_records_menu('user', array('deleted' => 0 , 'suspended' => 0), '','id, concat(firstname , lastname) as fullname');
            $select = $mform->addElement('select', 'user', get_string('user','supporttoolaction_sendmessage'), $users);
            $select->setMultiple(true);

            $mform->addElement('editor', 'body', get_string('body', 'supporttoolaction_sendmessage'));
            $mform->setType('body', PARAM_RAW);
        } else if($type == ACTION_BULK) {
            $mform->addElement('editor', 'body', get_string('body', 'supporttoolaction_sendmessage'));
            $mform->setType('body', PARAM_RAW);
        }
    }

    /**
     *  USER generate
     *
     * @param array [[userid,courseid,startdate,endate]]
     *
     *
     */
    public function manual_process($cir) {
        global $CFG ,$DB;
        $userval = $cir->user[0];
        $user = $DB->get_record('user', array('id' => $userval));
        $from = \core_user::get_support_user();
        $a = new \stdClass();
        $a->name = fullname($user);
        $a->contact = $from->email;
        $a->signature = fullname($from);
        $subject = get_string('subject', 'supporttoolaction_sendmessage', $a->name);
        $messagehtml = $cir->body['text'];
        $messagetext = format_text_email($messagehtml, FORMAT_HTML);
        $sentuser = email_to_user($user, $from, $subject, $messagetext, $messagehtml);
        echo '<div class="alert alert-success">Sucess</div>';
    }

    /**
     * Bluk process
     *
     */
    public function bulk_process($cir, $formdata) {
        global $CFG,$DB;
        // init csv import helper
        $cir->init();
        $linenum = 1;
        // init upload progress tracker
        $validation = array();
        while ($line = $cir->next()) {
            $linenum++;
            $user = new \stdclass();
            // add fields to user object
            foreach ($line as $keynum => $value) {
                $user = $DB->get_record('user',array('username' => $value));
                $from = \core_user::get_support_user();
                $a = new \stdClass();
                $a->name = fullname($user);
                $a->contact = $from->email;
                $a->signature = fullname($from);
                $subject = get_string('subject', 'supporttoolaction_sendmessage', $a->name);
                $messagehtml = $formdata->body['text'];

                print_object($messagehtml); die(); // DONOTCOMMIT
                $messagetext = format_text_email($messagehtml, FORMAT_HTML);
                $sentuser = email_to_user($user, $from, $subject, $messagetext, $messagehtml);
            }
            echo '<div class="alert alert-success">Sucess</div>';
        }
    }
}
